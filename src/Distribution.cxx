/**
 * @file Distribution.cxx
 * @author <a href="mailto:Andrea.Negri@pv.infn.it>Andrea Negri</a>
 * @author <a href="mailto:Per.Werner@cern.ch>Per Werner</a>
 * @version $Id$
 */

#include <stdlib.h>
#include <iostream>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "TF1.h"
#include "TH1.h"
#include "TFile.h"
#include "TROOT.h"
#include "TRandom.h"

#include "dccommon/Distribution.h"
#include "ers/ers.h"

// Static variables
unsigned int DC::Distribution::s_seed = 0;
std::atomic<bool> DC::Distribution::s_set_seed(true);

DC::Distribution::Distribution(const std::string& config, int32_t seed_param) 
: m_type(IllegalValue),
  m_configString(config),
  m_histogram(0),
  m_histoScale(1.0),
  m_function(0),
  m_min(1.0),
  m_max(0.0),
  m_constant(0)
{
  const std::string separator("|"); 
  std::string configWork = m_configString;
  // Divide the configString in tokens
  configWork.append(separator); 
  std::vector<std::string> tokens;
  for( std::string::size_type loc, prev = 0; 
    (loc = configWork.find(separator, prev)) != std::string::npos ; prev=loc+1) {
    if ((loc-prev) > 0) {
      tokens.push_back(configWork.substr(prev, loc-prev));
    }
  }
  if (tokens.size() < 2) {
    throw DC::DistributionIssue(ERS_HERE, ("Invalid configuration string '" + m_configString + "'").c_str()); 
  }
  // Call the appropriate configuration method
  bool ok=false;
  if (tokens[0]=="c" || tokens[0]=="C")      ok = configureConst(tokens);
  else if (tokens[0]=="f" || tokens[0]=="F") ok = configureTF1(tokens);
  else if (tokens[0]=="h" || tokens[0]=="H") ok = configureTH1(tokens); 
  if (s_set_seed) {
    s_set_seed = false;
    if (seed_param == 0) {
      auto now = std::chrono::high_resolution_clock::now(); 
      s_seed = now.time_since_epoch().count();
    } else if (seed_param < 0) {
      s_seed = getpid();
    } else {
      s_seed = seed_param;
    }
    gRandom->SetSeed(s_seed); 
    ERS_LOG("Root random generator 'gRandom' was seeded with " << s_seed << " from seed parameter " << seed_param);
  } else {
    ERS_LOG("Root random generator 'gRandom' was already seeded with " << s_seed);
  }
  if(!ok) {
    throw DC::DistributionIssue(ERS_HERE, ("Invalid configuration string '" + m_configString + "'").c_str());
  }
}

bool DC::Distribution::configureConst(const std::vector<std::string>& tokens)
{
  std::string usage("Usage: 'C|value'");
  if (tokens.size() != 2) {
    throw DC::DistributionIssue(ERS_HERE, "'Constant' distribution configuration has wrong number of tokens, e.g., 'C|value'");
  }
  m_constant = strtoul(tokens[1].c_str(), 0, 0);
  m_type = Constant;
  return true;
}

bool DC::Distribution::configureTF1(const std::vector<std::string>& tokens)
{
  ERS_DEBUG(1, "Configuring TF1 Distribution ...");
  std::string usage("Usage: 'F|TMath::Gaus(x,Mean,Sigma)|min|max'.\nNote that the user is responsable to match min and max values according to ROOT-function parameters.");
  if (tokens.size() != 4) {
    throw DC::DistributionIssue(ERS_HERE, ("'TF1' configuration syntax error. Wrong number of tokens: " + usage).c_str());
  }
  ERS_DEBUG(3, "Instantiate the TF1");
  m_min = atof(tokens[2].c_str());
  m_max = atof(tokens[3].c_str());
  if (m_min >= m_max || m_min < 0. || m_max < 0.) {
    throw DC::DistributionIssue(ERS_HERE, ("Configuring error. Invalid min/max limit: " + usage).c_str()); 
  }
  m_function = new TF1("Distribution", tokens[1].c_str(), m_min, m_max);
  if (!m_function) {
    throw DC::DistributionIssue(ERS_HERE, ("'TF1' Configuring error. Failed to create TF1 object: " + usage).c_str());  
  }
  m_type = TF;
  m_function->GetRandom();    // To trigger potential configuration procedures of root
  ERS_DEBUG(1, "Distribution TF1 configured: "
    << "\n\t TF1    = '" << tokens[1] << "'"
    << "\n\t Range  = " << m_min << " <--> " << m_max);
  return true;
}

bool DC::Distribution::configureTH1(const std::vector<std::string>& tokens)
{
  ERS_DEBUG(1, "Configuring TH1 Histogram Distribution '"
      << m_configString << "'");
  std::string usage("Usage: 'H|path-to-file.root|histogramDir|histogramName[|scaleFactor]'");
  
  if (tokens.size() != 5 && tokens.size() != 4) {
    std::ostringstream oss; 
    oss << "'TH1' Configuring syntax error: " << tokens.size()
          << " tokens found but 4 or 5 needed:\n" << usage;
    throw DC::DistributionIssue(ERS_HERE, oss.str().c_str());
  }
  ERS_DEBUG(1, "Distribution TH1 configuration parameters: "
    << "\n\t Root file    = '" << tokens[1] << "'"
    << "\n\t Histo dir    = '" << tokens[2] << "'"
    << "\n\t Histo name   = '" << tokens[3] << "'");

  // Open the root file
  TFile hfile(tokens[1].c_str());
  if (hfile.IsZombie()) {
    throw DC::DistributionIssue(ERS_HERE, ("Error opening the root file: '" + tokens[1] + "'").c_str());
  }
  // Set histogram directory if tokens[2] isn't empty. 
  if (tokens[2].size() && !hfile.cd(tokens[2].c_str())) {
    throw DC::DistributionIssue(ERS_HERE, ("Error opening the histogram  directory: '" + tokens[2] + "'").c_str());
  }
  // Find the histogram  
  TH1 *h = (TH1*) gDirectory->FindObjectAny(tokens[3].c_str());
  if (!h) {
    std::ostringstream oss; 
    oss << "Histogram '" << tokens[3] << "' not found in '" 
        << tokens[1] << "/" << tokens[2] << "'";
    throw DC::DistributionIssue(ERS_HERE, oss.str().c_str());
  }

  // Copy the histogram in memory
  m_histogram = (TH1*) h->Clone();
  m_histogram->SetDirectory(gROOT);
  hfile.Close();
  
  m_type = TH;
  if (tokens.size()==5) {
    char * ptr;
    double tmp = strtod(tokens[4].c_str(), &ptr);
    if (ptr == tokens[4].c_str()) throw DC::DistributionIssue(ERS_HERE, ("Bad scale factor: '" + tokens[4] + "'").c_str());
    m_histoScale = tmp;
  }

  ERS_DEBUG(1, "TH1 configuration: "
    << "\n\t Root file    = '" << tokens[1] << "'"
    << "\n\t Histo dir    = '" << tokens[2] << "'"
    << "\n\t Histo name   = '" << tokens[2] << "'"
    << "\n\t              = '" << m_histogram->GetName() << "'"
    << "\n\t Histo xmin   = "  << m_histogram->GetXaxis()->GetXmin()
    << "\n\t Histo xmax   = "  << m_histogram->GetXaxis()->GetXmax()
    << "\n\t Histo entries= "  << m_histogram->GetEntries()
    << "\n\t Scale factor = "  << m_histoScale
    << "\n\t Range        = "  << m_histogram->GetXaxis()->GetXmin()*m_histoScale 
    << " <-> " << m_histogram->GetXaxis()->GetXmax()*m_histoScale*m_histoScale);

  return true;
}

uint32_t DC::Distribution::next_value() const {
   // default is constant.
   uint32_t retval = 0xffffffff;
   switch (m_type) {
     case Constant:
       retval = m_constant;
       ERS_DEBUG(3, "next value: Constant " << retval);
     break;
     case TF:
        retval = static_cast<uint32_t>( m_function->GetRandom() + 0.5);
     break;
     case TH:
       retval = static_cast<uint32_t>((m_histogram->GetRandom() * m_histoScale));
       ERS_DEBUG(3, "next TH1." <<  m_histogram->GetName()
         << " value: " << " " << retval);
     break;
     default:
       std::ostringstream oss; 
       oss <<"Distribution::next_value(): bad configuration '"
         << m_configString << "', Distribution type=" << m_type
         << " (" << DistributionType2string(m_type) << ")"; 
       throw DC::DistributionIssue(ERS_HERE, oss.str().c_str()); 
     break;
   }
   return retval;
}

const std::string & DC::Distribution::DistributionType2string(DistributionType type) {
  static std::string types[] = { "Constant", "RootFunction", "RootHistogram", "IllegalValue" };
  if (type >= Constant && type < IllegalValue) {
    return types[type];
  }
  return types[IllegalValue];
}
