/* 
 * @author <a href="mailto:Per.Werner@cern.ch>Per Werner</a>
 * @version $Version$
 * @date Last updated by $Name$ at $Date$.
 * @brief Test class Distribution.
 */

#include <iostream>
#include <sstream>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "TH1I.h"
#include "TFile.h"
#include "TCanvas.h"

#include "dccommon/Distribution.h"

int retval = EXIT_SUCCESS;
bool print_help = false;

size_t count = 1000000;
std::string distributionConfig;
std::string rootFileName("");
bool timing = true;
int seed = 10;

void usage(char* argv0) {
  std::cout << "usage: " << argv0 << " -c <count> -d <config string> [-p] [-h]"
    << "\t-h                 print this help text\n"
    << "]\n\t-c <count>      no of values to generate [" << count
    << "]\n\t-d <'string'>   the quoted configuration string ['" << distributionConfig
    << "']\n\t-r <rootFile>   Root file for results [" << rootFileName
    << "]\n\t-s <seed>       seed value for random [" << seed << "]"<< std::endl;   
}

int main(int argc, char * argv[]) 
{
#if 0
  std::cerr << "Got the following " << argc << " arguments in argv"  << std::endl;
  for (int ix=1; ix < argc;) std::cerr << "argv[" << ix << "]='" << argv[ix] << "'\n";
#endif
  
  int optval;  
#if 0
  // getopt does not work when commandline contain quoted distribution strings.
 	while ((optval = getopt(argc, argv, "hc:d::r:s:")) != -1) {
  	switch (optval) {
#else 
  // Make my own decoding of options.
  for (int ix=1; ix < argc;) {
    if (argv[ix][0] != '-') {
      std::cerr << "FATAL: Unknown option '" << argv[ix] << "'" << std::endl;
      retval = EXIT_FAILURE;
      break;
    }
    char * optarg = 0;
    optval = argv[ix][1]; ix++;
    if (ix < argc && argv[ix][0] != '-') {
      optarg = argv[ix]; 
      ix++;
    }
#endif
  	switch (optval) {
    	case 'h':
        print_help = true; // parse all args before printing help
      break;
    	case 'c':
        count = strtoul(optarg, 0, 0);
      break;
    	case 'd':
        distributionConfig = optarg;
      break; 
    	case 'r':
        rootFileName = optarg;
        timing = false;
      break; 
    	case 's':
        seed = strtol(optarg, 0, 0);
      break; 
    	case '?':
      	std::cerr << "FATAL " << argv[0] << ": Unknown command option -'" 
          <<  optopt << "'" << std::endl;
        retval = EXIT_FAILURE;
        print_help = true; 
      break;
    	case ':':
      	std::cerr << "FATAL " << argv[0] 
          << ": Argument missing for command option -'" 
          <<  optopt << "'" << std::endl;
        retval = EXIT_FAILURE;
        print_help = true; 
      break;
      default:
      	std::cerr << "FATAL " << argv[0] << ": Unknown error for option '-" 
          << optval << "', optopt='" <<  optopt << "'" << std::endl;
        retval = EXIT_FAILURE;
        print_help = true; 
      break;
    }
  }
  if (print_help) {
    usage(argv[0]);
    return(retval);
  }
  
  TFile * rootFile = 0;
  try {
    DC::Distribution dist(distributionConfig, seed);
    if (dist.distributionType() == DC::Distribution::IllegalValue) {
      std::cerr << "ERROR: BAD Distribution configuration '" 
        << distributionConfig << "'." << std::endl;
      return EXIT_FAILURE;
    }
    
    if (dist.distributionType() == DC::Distribution::Constant) {
      volatile uint32_t value;
      std::chrono::time_point<std::chrono::high_resolution_clock> start =
        std::chrono::high_resolution_clock::now(); 
      for (size_t ix =0; ix<count; ix++) value = dist.next_value();
      unsigned int runTime_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(
          std::chrono::high_resolution_clock::now() - start).count();
      std::cout << "INFO: " << runTime_ns/count 
          << " ns/value when getting " << count << " values from " 
          << DC::Distribution::DistributionType2string(dist.distributionType())
          << " distribution '" << distributionConfig 
          << "' giving value=" << value << "." << std::endl;
    } else {
      if (timing) { // Do timing only
        volatile uint32_t value;
        std::chrono::time_point<std::chrono::high_resolution_clock> start =
          std::chrono::high_resolution_clock::now(); 
        for (size_t ix =0; ix<count; ix++)  value = dist.next_value();
        unsigned int runTime_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(
          std::chrono::high_resolution_clock::now() - start).count();
        std::cout << "INFO: " << runTime_ns/count << " ns/value when getting " 
            << count << " values from " 
            << DC::Distribution::DistributionType2string(dist.distributionType())
            << " distribution '" << distributionConfig << "'." << std::endl;
      } else {
        // Open Root file if necessary
        if (rootFile == 0) {
          rootFile = new TFile(rootFileName.c_str(), "UPDATE");
          if (rootFile->IsZombie()) {
            std::cerr << "ERROR opening file=" << rootFileName << std::endl;
            if (rootFile && rootFile->IsOpen()) rootFile->Close();
            return EXIT_FAILURE;
          }
        }
        // Create a histogram with dynamic bins.
        TH1I histo(DC::Distribution::DistributionType2string(dist.distributionType()).c_str(),
            (distributionConfig).c_str(), 200, 1, 0);
        std::chrono::time_point<std::chrono::high_resolution_clock> start =
          std::chrono::high_resolution_clock::now(); 
        for (size_t ix =0; ix<count; ix++) histo.Fill(dist.next_value());
        unsigned int runTime_ns = std::chrono::duration_cast<std::chrono::nanoseconds>(
          std::chrono::high_resolution_clock::now() - start).count();
        std::cout << "INFO: " << runTime_ns/count << " ns/value when filling histogram with " 
            << count << " values from " 
            << DC::Distribution::DistributionType2string(dist.distributionType())
            << " distribution '" << distributionConfig << "'." << std::endl;
        rootFile->cd();
        //TCanvas * c1 = new TCanvas("Test DC::Distribution class", (distributionConfig).c_str(), 1);
        histo.Draw();
        rootFile->Write(DC::Distribution::DistributionType2string(dist.distributionType()).c_str()); 
      }
    }
  }
  catch (DC::DistributionIssue &ex)
  { std::cerr << "Caught DC::DistributionIssue: " << ex.what() << std::endl; exit(EXIT_FAILURE); }
  catch (ers::Issue &ex)
  { std::cerr << "Caught ers::Issue: " << ex.what() << std::endl; exit(EXIT_FAILURE); }
  catch (std::exception &ex)
  { std::cerr << "Caught std::exception: " << ex.what() << std::endl; exit(EXIT_FAILURE); }

  if (rootFile) {
    rootFile->Close();
  }
  return retval;
}
