#!/usr/bin/env tdaq_python

"""
$Id$
Author: Per Werner, PH-ADT, CERN, Mar-2012.

Script to run tests of the Distribution class.
usage: test_Distribution.py options
"""
import os, getopt, sys

from optparse import OptionParser

parser = OptionParser()
parser.add_option('-v', dest='verbose', action="store_true", default=False, help='verbose output')
parser.add_option('-C', '--constant', dest='constant', type='string', default='C|1248', help='Constant value ["C|1248"]')
parser.add_option('-c', '--count', dest='count', type='int', default=1000000, help='value count [1000.000]')
parser.add_option('-F', '--funcConfig', dest='funcConfig', type='string', default='F|TMath::Gaus(x,1500,200)|1000|2000', help='The ROOT function config string ["F|TMath::Gaus(x,1500,200)|1000|2000"]')
parser.add_option('-H', '--histoConfig', dest='histoConfig', type='string', default='H|./timeDist.root|/|ProcTimeDist', help='ROOT histogram config string ["H|./timeDist.root|/|ProcTimeDist"]')
parser.add_option('-S', '--scaleFactor', dest='scaleFactor', type='int', default=1, help='Scale factor for ROOT histogram [1]')
parser.add_option('-r', '--rootFile', dest='rootFile', type='string', default='RESULTS.root', help='ROOT file for results [./RESULTS.root]')
parser.add_option('-s', '--seed', dest='seed', type='int', default=10, 
   help='seed value for random function [10]')
parser.add_option('-t', '--timing', dest='timing', action='store_true', default=False, help='timing only (no ROOT file written)')

pgm='test_Distribution'

# main program
if __name__ == '__main__':
  (options, args) = parser.parse_args(sys.argv[1:])
  if options.verbose: print "timing=%s, verbose=%s, sys.argv[1:]='%s'" % (options.timing, options.verbose, sys.argv[1:])
  
  command_base =  pgm + ' -c ' + str(options.count) + ' -s ' + str(options.seed) 
  if not options.timing: 
    if len(options.rootFile): 
      command_base = command_base + ' -r ' + options.rootFile 
      try: os.remove(options.rootFile)
      except: pass
  print "INFO: ROOT histogram file for results is '%s'" % options.rootFile
  command_base = command_base + ' -d '
  
  exit_status = 0
  used_quote="\'"
  funcConfig = used_quote + options.funcConfig + used_quote
  histoConfig = used_quote + options.histoConfig +str(' | %d' % options.scaleFactor) + used_quote
  if options.verbose: print "scaleFactor=%s,  histoConfig=%s" % (str(' | %d' % options.scaleFactor), histoConfig)
  if len(options.constant): 
    distribution = used_quote + options.constant + used_quote
    if options.verbose: print "INFO: Execute command: %s%s" % (command_base, options.constant)
    status = os.system(command_base + distribution)
    if status: 
      exit_status = exit_status+1
      print "ERROR: Test of distribution '%s' failed with status %d" % (distribution, status)
    elif options.verbose: print "INFO: Status=%d for command: %s%s" % (status, command_base, distribution)
  
  if len(options.funcConfig): 
    distribution = used_quote + options.funcConfig + used_quote
    if options.verbose: print "INFO: Execute command: %s%s" % (command_base, distribution)
    status = os.system(command_base + distribution)
    if status: 
      exit_status = exit_status+1
      print "ERROR: Test of distribution '%s' failed with status %d" % (distribution, status)
    elif options.verbose: print "INFO: Status=%d for command: %s%s" % (status, command_base, distribution)
  
  if len(options.histoConfig): 
    distribution = used_quote + options.histoConfig + used_quote
    if options.verbose: print "INFO: Execute command: %s%s" % (command_base, distribution)
    status = os.system(command_base + distribution)
    if status: 
      exit_status = exit_status+1
      print "ERROR: Test of distribution '%s' failed with status %d" % (distribution, status)
    elif options.verbose: print "INFO: Status=%d for command: %s%s" % (status, command_base, distribution)
  
  sys.exit(exit_status) 
