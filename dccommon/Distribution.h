#ifndef DCCOMMON_DISTRIBUTION_H
#define DCCOMMON_DISTRIBUTION_H
/**
 * @author <a href="mailto:Andrea.Negri@pv.infn.it>Andrea Negri</a>
 * @author <a href="mailto:Per.Werner@cern.ch>Per Werner</a>
 * $Id$
 *
 * @brief This code was initially copied from the 
 * PT package provided by A.Negri.
 * This class provides a number distribution depending 
 * on the Distribution configuration string. 
 * The configuration string consists of 2-4 tokens 
 * separated by a vertical bar, '|'.
 *
 * The number can be a
 * <ol>
 * <li> constant, e.g., "C|450". </li> 
 * <li> ROOT function, "F|<root-function>|<min>|<max>", e.g., "F|TMath::Gaus(x,250,0)|50|520". </li> 
 * <li> ROOT histogram, "H|/<path-to-root-file>|<histo-path-in-root-file>|<scale-factor>",
 * e.g., "H|/dir-path/file.root|/in-file/path|1.0". </li> 
 * </ol>
 *
 * The values returned by the Distribution is of type uint32_t.
 */
 
#include <string>
#include <vector>
#include <atomic>

#include "ers/ers.h"

ERS_DECLARE_ISSUE_BASE(
  DC,
  DistributionIssue,
  ers::Issue,
  "" << reason,
  ERS_EMPTY,
  ((const char*) reason)
)

class TF1;
class TH1;

namespace DC {
  class Distribution {
    public:
      typedef enum {Constant=0, TF, TH, IllegalValue} DistributionType;

      /**
       * Constructor: 
       * @param configString The string that defines the type 
       * of distribution to be used. 
       * The values returned are integer 32 bit type.
       * @throws DistributionIssue
       * The followoing distributions can be defined:
       * <ol>
       * <li> "C|n", <b>constant</b> value n.
       * </li>
       * <li> "F|rootFuntion|min|max", the values are generated 
       * by the ROOT function <b>rootFuntion</b> (with parameters) 
       * where <b>min</b> and <b>max</b> are delimiting the returned 
       * values.
       * </li>
       * <li> "H|path-to-file|histogramDir|histogramName[|scaleFactor=1]",
       * the values are retrieved from the ROOT histogram where
       * <b>path-to-file</b> is the path to the ROOT file.
       * </li>
       * </ol>
       * @param seed Set the seed for the ROOT gRandom random generator
       * <b>the first time this constructor is called in a process</b>.
       * If seed is
       * <ol> 
       * <li> > 0 seed with with the parameter value. </li>
       * <li> == 0 the tick count of the machine time is used as a seed. </li>
       * <li> < 0 seed with the process ID. </li>      
       * </ol>
       */
      Distribution(const std::string & configString, int32_t seed = -1);
      
      /** Get next value */
      uint32_t next_value() const;
      
      /** Get the Distribution configuration string */
      inline const std::string & get_DistributionString() const { return m_configString; }
      
      /** Get name of a DistributionType */
      static const std::string & DistributionType2string(DistributionType type);
      
      /** 
       * Get the DistributionType. 
       *  @return the DistributionType for this Distribution.
       */
      inline DistributionType distributionType() const { return m_type; }
      
    protected: ///< Internal use only
      
      // Configure a constant
      bool configureConst(const std::vector<std::string> & tokens);
  
      // Configure a root function to generate the distribution
      bool configureTF1(const std::vector<std::string> & tokens);
  
      /// Configure a root histrogram to generate distribution
      bool configureTH1(const std::vector<std::string> & tokens);
    
    private:
      DistributionType     m_type; ///< type of number distribution: Constant, function or histogram.       
      std::string m_configString;  ///< Distribution configuration string.
      TH1 *    m_histogram;        ///< Histogram used to generate number distribution.
      double   m_histoScale;       ///< Used to rescale the histogram.
      TF1 *    m_function;         ///< Function used to generate number distribution.
      double   m_min;              ///< The low end for generated numbers.
      double   m_max;              ///< The high end for generated numbers.
      uint32_t m_constant;         ///< If constant.
      static unsigned int s_seed;  ///< Global seed that was used to set ROOT grandom. Invalidated if grandom renewed.
      static std::atomic<bool> s_set_seed; ///< Only the first constructed Distribution in a process sets grandom.
  }; 
} 

#endif // DCCOMMON_DISTRIBUTION_H
