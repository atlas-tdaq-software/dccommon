#Test of the Distribution class
===============================
# $Id$

Build dccommon.
cd WORK/dccommon/test

# The test below gives timing for the three distribution types for 10M values. RESULTS.root contain the ROOT histograms.
./test_Distribution.py -C  'C|1248' -F 'F|TMath::Gaus(x,1600,200)|900|2300' -H 'H|./timeDist.root|/|ProcTimeDist' -c 10000000

# Make histograms for a ROOT function and a histogram with different scaling for 10M values.
test_Distribution -d 'F|TMath::Gaus(x,1600,200)|900|2300'  -c 10000000 -r Function.root
test_Distribution -d 'H|./timeDist.root|/|ProcTimeDist' -c 10000000 -r Histo.root
test_Distribution -d 'H|./timeDist.root|/|ProcTimeDist|2.0' -c 10000000 -r HistoScale2.root

#look at the ROOT files.
root
TBrowser b
.q

# Generate some error conditions
test_Distribution -d  'C|' 
test_Distribution -d  'x|12' 
test_Distribution -d 'F|TMath::Gaus(x,1600,200)|2300|900'  -c 10000000 
test_Distribution -d 'H|./timeDistX.root|/|ProcTimeDist' -c 10000000 
test_Distribution -d 'H|./timeDist.root|/|ProimeDist' -c 10000000 
test_Distribution -d 'H|./timeDist.root|/|ProcTimeDist|xxx' -c 10000000 

# Note: Conecutive separators are ignored.
test_Distribution -d  'C||12' 

